/**
 * @file
 * Marker Popup.
 */

/**
 * @typedef {Object} LeafletMarkerPopupSettings
 *
 * @extends {GeolocationMapFeatureSettings}
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Marker Popup.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches common map marker popup functionality to relevant elements.
   */
  Drupal.behaviors.geolocationAjaxPopup = {
    attach: function (context, drupalSettings) {
      Drupal.geolocation.executeFeatureOnAllMaps('leaflet_ajax_popup', function (map, featureSettings) {
        map.leafletMap.on('popupopen', function(e) {
          let element = e.popup._contentNode;
          let content = $('[data-geolocation-ajax-popup]', element);
          if (content.length) {
            let url = content.data('geolocation-ajax-popup');
            Drupal.ajax({url: url}).execute().done(function () {

              // Copy the html we received via AJAX to the popup, so we won't
              // have to make another AJAX call (#see 3258780).
              e.popup.setContent(element.innerHTML);

              // Call update() so Leaflet refreshes the map, panning it if
              // necessary to bring the full popup into view (#see 3258780).
              e.popup.update();

              // Attach drupal behaviors on new content.
              Drupal.attachBehaviors(element, drupalSettings);
            });
          }
        });

        return true;
      }, drupalSettings);
    },
    detach: function (context, drupalSettings) {}
  };
})(jQuery, Drupal);
