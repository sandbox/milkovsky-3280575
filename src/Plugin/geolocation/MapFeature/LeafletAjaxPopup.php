<?php

namespace Drupal\geolocation_ajax_popup\Plugin\geolocation\MapFeature;

use Drupal\geolocation\MapFeatureFrontendBase;

/**
 * Provides disabled interaction.
 *
 * @MapFeature(
 *   id = "leaflet_ajax_popup",
 *   name = @Translation("Marker popup - AJAX"),
 *   description = @Translation("Enable to load popup content via AJAX."),
 *   type = "leaflet",
 * )
 */
class LeafletAjaxPopup extends MapFeatureFrontendBase {

}
