<?php

namespace Drupal\geolocation_ajax_popup\Plugin\views\style;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\geolocation\Plugin\views\style\CommonMap;
use Drupal\geolocation_ajax_popup\Controller\GeolocationAjaxPopupController;
use Drupal\views\Plugin\views\PluginBase;
use Drupal\views\ResultRow;

/**
 * Allow to display several field items on a common map with AJAX popups.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "maps_common_ajax",
 *   title = @Translation("Geolocation CommonMap (AJAX)"),
 *   help = @Translation("Display geolocations on a common map."),
 *   theme = "views_view_list",
 *   display_types = {"normal"},
 * )
 */
class CommonMapAjax extends CommonMap {

  public function render() {
    $element = parent::render();
    $element['#attached']['library'][] = 'geolocation_ajax_popup/geolocation-ajax-popup';
    $build_for_bubbleable_metadata['#attached']['library'][] = 'core/drupal.ajax';
    BubbleableMetadata::createFromRenderArray($element)
      ->merge(BubbleableMetadata::createFromRenderArray($build_for_bubbleable_metadata))
      ->applyTo($element);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function getLocationsFromRow(ResultRow $row) {
    $build = parent::getLocationsFromRow($row);

    $entity = $row->_entity;
    $entity_type = $entity->getEntityTypeId();
    $entity_id = $entity->id();
    $view_mode = 'teaser';
    $langcode = $this->getLangcode($entity);
    $parameters = [
      'entity_type' => 'node',
      'entity' => $entity_id,
      'view_mode' => $view_mode,
      'langcode' => $langcode,
    ];
    $url = Url::fromRoute('geolocation_ajax_popup.ajax_popup', $parameters)->toString();
    $popup = GeolocationAjaxPopupController::getPopupIdentifierAttribute($entity_type, $entity_id, $view_mode, $langcode);
    $content = '<div class="geolocation-ajax-popup" data-geolocation-ajax-popup="' . $url . '" ' . $popup . '></div>';
    $build[0]['content'] = [
      '#markup' => $content,
    ];

    return $build;
  }

  /**
   * Gets langcode.

   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The langcode.
   */
  protected function getLangcode(EntityInterface $entity) {
    $entity_type_langcode_attribute = $entity->getEntityTypeId() . '_field_data_langcode';
    $rendering_language = $this->view->display_handler->getOption('rendering_language');
    $dynamic_renderers = [
      '***LANGUAGE_entity_translation***' => 'TranslationLanguageRenderer',
      '***LANGUAGE_entity_default***' => 'DefaultLanguageRenderer',
    ];
    if (isset($dynamic_renderers[$rendering_language])) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      return isset($result->{$entity_type_langcode_attribute}) ? $result->{$entity_type_langcode_attribute} : $entity->language()->getId();
    }
    else {
      if (strpos($rendering_language, '***LANGUAGE_') !== FALSE) {
        return PluginBase::queryLanguageSubstitutions()[$rendering_language];
      }
      else {
        // Specific langcode set.
        return $rendering_language;
      }
    }

    return NULL;
  }

}
